import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  TouchableOpacity,
  Image,
  SafeAreaView,
  TextInput,
  ScrollView,
} from 'react-native';
const Splash = ({ navigation }) => {
  return (
      <View style={styles.container}>
        <Image
          
           source={{
          uri: 'https://wallpapercave.com/wp/wp7632525.jpg',
        }}
          resizeMode="contain"
          style={{
            resizeMode: 'cover',
            height: 200,
            width: 200,
          }}
        />
      </View>
  )
};
export default Splash;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    paddingTop: 95,
  },
});