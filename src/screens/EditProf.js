import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { RadioButton } from 'react-native-paper';
import DatePicker from 'react-native-datepicker'
const EditProfile = () =>{
  const [value, setValue] = React.useState('first');
  const state = {date:"2016-05-15"};
  return(
    <ScrollView style={styles.scrollView}>
    <View style={styles.regform}>
    <Text>{" "}</Text>
    <Text>{" "}</Text>
    <Text style={styles.header}> User Profile </Text>
    <Text style={styles.textcolor}>First Name </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your firstname" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Last Name </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your lastname" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Father Name </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your fathername" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Age </Text>
    <DatePicker
        style={{width: 250}}
        date={state.date}
        mode="date"
        placeholder="select date"
        format="DD-MM-YYYY"
        minDate="01-01-1990"
        maxDate="01-01-2021"
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: 'absolute',
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={(date) => {setState({date: date})}}
      />
    <Text style={styles.textcolor}>Gender </Text>
    <RadioButton.Group onValueChange={value => setValue(value)} value={value}>
      <RadioButton.Item label="Male" value="first" />
      <RadioButton.Item label="Female" value="second" />
      <RadioButton.Item label="Others" value="third" />
    </RadioButton.Group>
    <Text style={styles.textcolor}>Phone no 1 </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your Phone no 1" underlineColorAndroid={'transparent'}/>    
    <Text style={styles.textcolor}>Phone no 2 </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your Phone no 2" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Email ID </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your email id" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Address - 1 </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your address 1" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Address - 2 </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your address 2" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>District </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your district" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Pin code </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your pin code" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Education </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your education" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>College </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your college" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Completed year </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your academiccompleted year" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Present Profession </Text> 
    <TextInput style={styles.textinput} placeholder="Enter your present profession" underlineColorAndroid={'transparent'}/>
    <Text style={styles.textcolor}>Interested in Carrier</Text> 
    <TextInput style={styles.textinput} placeholder="Enter your Interested Carrier" underlineColorAndroid={'transparent'}/>
    <TouchableOpacity style={styles.appButtonContainer}>
            <Text
              style={styles.appButtonText}
              secureTextEntry={true}
              color="grey"
              align="center"
              onPress={() => console.log('start')}>
              Submit
            </Text>
          </TouchableOpacity>
    </View>
    </ScrollView>
  );
}
export default EditProfile;
const styles= StyleSheet.create({
  regform: {
    alignSelf:'stretch',
  },
  header: {
    fontSize:30,
    color:'#0f73ee',
    paddingBottom:5,
    marginBottom:20,
    paddingLeft: 60,
    //paddingRight:10,
    //borderBottomColor:'blue',
    //borderBottomWidth:2,
  },
  textinput: {
    alignSelf:'stretch',
    height: 40,
    marginBottom: 30,
    color: 'black',
    borderBottomColor:'#A9A9A9',
    borderBottomWidth:2,
  },
  scrollView: {
    //backgroundColor: '#6ED4C8',
    marginHorizontal: 5,
    paddingLeft:20,
    paddingRight:30,
  },
  button:{
    alignSelf:'stretch',
    alignItems:'center',
    padding:5,
    backgroundColor: 'purple',
    marginTop:20,
    marginBottom:20,
  },
  appButtonContainer: {
    elevation: 6,
    backgroundColor: '#0f73ee',
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 15,
    width:300,
    height:40,
    marginBottom:20,
  },
  appButtonText: {
    fontSize: 20,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  textcolor:{
    color: '#808080',
  }
});