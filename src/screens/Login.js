import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  TouchableOpacity,
  Image,
  SafeAreaView,
  TextInput,
  ScrollView,
} from 'react-native';
const Login = ({ navigation }) => {
  const Bold = ({ children }) => (
    <Text style={{ fontWeight: 'bold', color: 'black' }}>{children}</Text>
  );
  return (
    <ScrollView>
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 12,
            height: 80,
            alignItems: 'flex-end',
          }}>
          <TouchableOpacity
            style={{ marginLeft: 8 }}
            onPress={() => navigation.goBack()}>
            <Image
              source={require('../assets/images/back_arrow_icon.png')}
              resizeMode="contain"
              style={{
                width: 25,
                height: 25,
              }}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.header}>
          <Image
            source={require('../assets/images/get_otp.png')}
            resizeMode="contain"
            style={{
              borderBottomLeftRadius: 30,
              height: 200,
              width: 200,
            }}
          />
          <Text style={styles.text_footer}>OTP Verification</Text>
        </View>
        <View style={styles.footer}>
          <Text style={styles.in_footer}>
            We will send you an <Bold>One Time Password</Bold>
          </Text>
          <Text style={styles.on_footer}>on this mobile number</Text>
          <Text> </Text>
          <Text> </Text>
          <Text style={styles.on_footer}>Enter Mobile Number</Text>
          <TextInput
            style={styles.textinput}
            underlineColorAndroid={'transparent'}
          />
          <TouchableOpacity style={styles.appButtonContainer}>
            <Text
              style={styles.appButtonText}
              secureTextEntry={true}
              color="grey"
              align="center"
              onPress={() => navigation.navigate('Otp')}>
              Get otp
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};
export default Login;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FEFEFA',
    justifyContent:'flex-start',
    alignItems:'center'
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 70,
    paddingTop: 1,
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 100,
    borderTopRightRadius: 100,
    paddingHorizontal: 30,
    paddingVertical: 30,
    paddingBottom: 70,
  },
  textinput: {
    alignSelf: 'stretch',
    height: 55,
    marginBottom: 30,
    color: 'black',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    paddingBottom: 8,
    borderBottomColor: '#A9A9A9',
    borderBottomWidth: 2,
    maxWidth: 500,
  },
  text_header: {
    color: '#0f73ee',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_footer: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 25,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
    textAlign: 'center',
  },
  in_footer: {
    color: '#A9A9A9',
    fontWeight: 'normal',
    fontSize: 15,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
  },
  on_footer: {
    color: '#A9A9A9',
    fontWeight: 'normal',
    fontSize: 17,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
    textAlign: 'center',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  appButtonContainer: {
    elevation: 6,
    backgroundColor: '#0f73ee',
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 15,
    width: 300,
    height: 50,
  },
  appButtonText: {
    fontSize: 25,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
    textTransform: 'uppercase',
  },
});