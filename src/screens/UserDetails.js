// import React from 'react';
// import {
//   View,
//   Text,
//   ImageBackground,
//   TouchableOpacity,
//   Image,
//   ScrollView,
//   Animated,
//   Alert,
//   StyleSheet, AsyncStorage
// } from 'react-native';
// import { delUser } from '../services/Apiservices'
// import RoundedButton from '../components/RoundedButton';
// const ListUserDetail = ({ route, navigation }) => {
//   const [book, setBook] = React.useState(' ');
//   const [jwt_token, setJwt] = React.useState(' ');

//   React.useEffect(() => {
//     let { book } = route.params;
//     setBook(book);

//     AsyncStorage.getItem('userToken').then(async (res) => {
//       const jwt = await res;
//       // console.warn("get async", t);
//       setJwt(jwt);

//     });

//   }, [book]);
//   console.log(book);


//   const delete_user = () => {
//     console.log(book.userId);

//     delUser('delete_user/' + book.userId + '/V1.0', jwt_token)
//       .then((response) => {

//         if (response.status == 200) {
//           Alert.alert("User Deleted Successfully");
//           navigation.goBack();
//         }

//       })
//       .catch((error) => {

//         Alert.alert("Something went wrong, Try again");

//       });

//   }

//   return (
//     <View>

//       <View
//         style={{
//           flexDirection: 'column',
//           paddingHorizontal: 12,
//           height: 50,
//           alignItems: 'center',
//         }}>
//         <Text style={styles.text}>
//           <Image
//             style={{ height: 20, width: 20 }}
//             source={{
//               uri:
//                 'https://research.cbc.osu.edu/sokolov.8/wp-content/uploads/2017/12/profile-icon-png-898.png',
//             }}
//           />{' '}
//           {book.firstName}
//           <Text> </Text>
//         </Text>
//         <Text style={styles.text2}>
//           <Image
//             style={{ height: 20, width: 20 }}
//             source={{
//               uri: 'https://www.iconsdb.com/icons/preview/gray/email-2-xxl.png',
//             }}
//           />{' '}
//           {book.email}{' '}
//         </Text>
//         <Text style={styles.text2}>
//           <Image
//             style={{ height: 20, width: 20 }}
//             source={{
//               uri:
//                 'https://cdn3.iconfinder.com/data/icons/maps-and-navigation-colored-1/48/37-512.png',
//             }}
//           />{' '}
//           {book.city} <Text> </Text>
//           <Image
//             style={{ height: 20, width: 20 }}
//             source={{
//               uri:
//                 'https://cdn2.iconfinder.com/data/icons/gaming-and-beyond-part-2-1/80/Phone_gray-512.png',
//             }}
//           />
//           <Text> </Text>
//           {book.phoneNumber}
//         </Text>

//         <View >
//           <RoundedButton 
//           label={  "Delete User"}
//           onPress={() => delete_user()}
//           bgColor ={'red'} 
//           style={{margin:5,padding:5,paddingLeft:10,paddingRight:10,borderRadius:5}}>

//           </RoundedButton>
//         </View>

//       </View>


//     </View>
//   );
// };
// export default ListUserDetail;
// const styles = StyleSheet.create({
//   container: {
//     backgroundColor: '#F5FCFF',
//     alignContent: 'center',
//     alignItems: 'center',
//   },
//   text: {
//     marginLeft: 12,
//     fontSize: 20,
//   },
//   text2: {
//     marginLeft: 12,
//     fontSize: 16,
//     color: 'gray',
//   },
// });
import React from 'react';
import {
  View,
  Text,
  AsyncStorage,
  Image,
  ScrollView,
  Animated,
  Alert,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { delUser } from '../services/Apiservices'
import CommonStyles from '../constants/styles';
import RoundedButton from '../components/RoundedButton';
import Moment from 'moment';

const ListUserDetail = ({ route, navigation }) => {
  const [book, setBook] = React.useState(' ');
  const [book1, setBook1] = React.useState(' ');
  const [loc1, setloc1] = React.useState(' ');
  const [empd, setempd] = React.useState(' ');
  const [empl, setempl] = React.useState(' ');
  const [jwt_token, setJwt] = React.useState(' ');
  React.useEffect(() => {
    let { book } = route.params;
    setBook(book);
    setBook1(book.educationDetails[0]);
    setloc1(book.educationDetails[0].location);
    setempd(book.employmentDetails[0]);
    setempl(book.employmentDetails[0].location);
    AsyncStorage.getItem('userToken').then(async (res) => {
      const jwt = await res;
      setJwt(jwt);
    });
  }, [book]);
  Moment.locale('en');
const delete_user = () => {
    delUser('delete_user/' + book.userId + '/V1.0', jwt_token)
      .then((response) => {
        if (response.status == 200) {
          Alert.alert("User Deleted Successfully");
          route.params.onGoBack();
          navigation.goBack();
        }
      })
      .catch((error) => {
        Alert.alert("Something went wrong, Try again");
      });
}

return (
    <ScrollView>
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 12,
            alignItems: 'flex-end',
          }}>
            <Text>{' '}</Text>
          <TouchableOpacity
                style={styles.button}
                onPress={() =>
                    navigation.navigate('EditUserProfile', {
                        user: book,
                    })
                }>
              <Text
              style={styles.appButtonText1}
              secureTextEntry={true}
              color="grey"
              align="center"
              >
              Edit User
            </Text>
                </TouchableOpacity>
        </View>
        <View style={styles.header}>
          <View>
            <Text>Name:</Text>
            <Text style={styles.text_header}>
               {book.firstName} {book.lastName} 
            </Text>
          </View>
          <View>
            <Text>FatherName:</Text>
            <Text style={styles.text_header}>

              {book.middleName}
            </Text>
          </View>
          <View>
            <Text>Email:</Text>
            <Text style={styles.text_header}>
              {book.email}
            </Text>
          </View>
          <View>
            <Text>City/Town:</Text>
            <Text style={styles.text_header}>
              {book.ondriyam}
            </Text>
          </View>
          <View>
            <Text>Phone:</Text>
            <Text style={styles.text_header}>
             {book.phoneNumber},{book.alternatePhone}
            </Text>
          </View>
          <View>
            <Text>Gender:</Text>
            <Text style={styles.text_header}>
                {book.gender}
            </Text>
          </View>
          <View>
            <Text>Age:</Text>
            <Text style={styles.text_header}>
              {Moment().diff(book.birthDate, 'years', false)} years
            {/* {Moment(book.birthDate).fromNow()} */}
            </Text>
          </View>
          <View>
            <Text>address1:</Text>
            <Text style={styles.text_header}>
              {book.address1}
            </Text>
          </View>
            <View>
            <Text>address2:</Text>
            <Text style={styles.text_header}>
            {book.address2}
            </Text>
          </View>
          <View>
            <Text>state:</Text>
            <Text style={styles.text_header}>
            {book.state}
            </Text>
          </View>
          <View>
            <Text>district:</Text>
            <Text style={styles.text_header}>
            {book.district}
            </Text>
          </View>
          <View>
            <Text>ondriyam:</Text>
            <Text style={styles.text_header}>
              {book.ondriyam}
            </Text>
          </View>
          <View>
            <Text>area:</Text>
            <Text style={styles.text_header}>
              {book.area}
            </Text>
          </View>
            <View>
            <Text>location:</Text>
            <Text style={styles.text_header}>
              {book.location}
            </Text>
          </View>
            <View>
            <Text>pincode:</Text>
            <Text style={styles.text_header}>
              {book.pincode}
            </Text>
          </View>
          <View>
            <Text>{' '}</Text>
            <Text style={styles.text_header}>
              Education Details:
            </Text>
          </View> 
           <View>
            <Text>Domain:</Text>
            <Text style={styles.text_header}>
              {book1.domain}
            </Text>
          </View>
          <View>
            <Text>School/College Name:</Text>
            <Text style={styles.text_header}>
            {book1.name}
            </Text>
          </View>
           <View>
            <Text>city:</Text>
            <Text style={styles.text_header}>
             {loc1.city}
            </Text>
          </View>
          <View>
            <Text>state:</Text>
            <Text style={styles.text_header}>
           {loc1.state}
            </Text>
          </View> 
          <View>
            <Text>{' '}</Text>
            <Text style={styles.text_header}>
              EmploymentStatus:
            </Text>
            <Text style={styles.text_header}>
           {book.employmentStatus}
            </Text>
          </View>
          <View>
            <Text>{' '}</Text>
            {book.employmentStatus=='Employed' ? 
            <View>
            <Text style={styles.text_header}>
              Employement Details:
            </Text>
             <Text>company name:</Text>
            <Text style={styles.text_header}>
              {empd.comapnyName}
            </Text>
            <Text>designation:</Text>
            <Text style={styles.text_header}>
              {empd.designation}
            </Text>
             <Text>Experipence:</Text>
            <Text style={styles.text_header}>
              {empd.period}
            </Text>
             <Text>city:</Text>
            <Text style={styles.text_header}>
           {empl.city}
            </Text>
             <Text>state:</Text>
            <Text style={styles.text_header}>
           {empl.state}
            </Text>
            </View>
            :null}
            </View>
          <View></View>
            <RoundedButton
              label={"Delete User"}
              onPress={() => delete_user()}
              bgColor={'red'}
              style={{ margin: 5, padding: 7, paddingLeft: 10, paddingRight:10, borderRadius: 5 }}>
            </RoundedButton>
          </View>
      </View>
    </ScrollView>
  );
};
export default ListUserDetail;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: CommonStyles.color.COLOR_BACKGROUND_PRIMARY,
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 10,
    paddingTop: 1,
    backgroundColor: '#FFFF',
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 100,
    borderTopRightRadius: 100,
    paddingHorizontal: 30,
    paddingVertical: 30,
    paddingBottom: 70,
    justifyContent: 'flex-end',
  },
  textinput: {
    alignSelf: 'stretch',
    height: 55,
    color: 'black',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
    borderBottomColor: '#A9A9A9',
    borderBottomWidth: 2,
  },
  textViewContainer:{
    padding:5,
    marginTop:5
  },
  text_header: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 18,
  },
  text2: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
  },
  errorMsg: {
    color: '#b20000',
    fontSize: 14,
    textAlign: 'center',
  },
  text_footer: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 25,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
    textAlign: 'center',
    justifyContent: 'flex-end',
  },
  in_footer: {
    color: '#A9A9A9',
    fontWeight: 'normal',
    fontSize: 15,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
  },
  on_footer: {
    color: '#A9A9A9',
    fontWeight: 'normal',
    fontSize: 17,
    textShadowColor: '#D50000',
    fontFamily: 'sans-serif',
    textAlign: 'center',
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  appButtonContainer: {
    elevation: 6,
    backgroundColor: '#0f73ee',
    borderRadius: 6,
    paddingVertical: 8,
    paddingHorizontal: 15,
    width: 300,
    height: 50,
  },
  appButtonText: {
    fontSize: 25,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
    textTransform: 'uppercase',
  },
  button:{
  elevation: 6,
    backgroundColor: 'red',
    borderRadius: 10,
    paddingVertical: 5,
    paddingHorizontal: 3,
    width:100,
    height:30,
    marginBottom:10,
    marginLeft:240,
    marginRight:190, 
  },
  appButtonText1: {
    fontSize: 15,
    color: '#fff',
    fontWeight: 'bold',
    alignSelf: 'center',
    paddingRight: 3,
    paddingLeft: 3,
    paddingBottom:10,

  },
});