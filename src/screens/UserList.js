import React from 'react';
import {
    Button,
    Text,
    StyleSheet,
    View,
    FlatList,
    Image,
    TouchableOpacity,
    TouchableHighlight,
    Modal,
    Alert,
    TextInput,
    Picker,AsyncStorage,
    ScrollView
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import Icons from 'react-native-vector-icons/MaterialIcons';
import { getAllData } from '../services/Apiservices'
class ListUser extends React.Component {
    state = {
        modalVisible: false,
        dataSource: [],
        firstName: ' ',
        gender: ' ',
        employmentStatus: ' ',
        boothName: [],
        arr: [],
        booths: [],
    };
    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    };
    _onPressBack() {
        const { goBack } = this.props.navigation;
        goBack();
    }
    onChangeHandle(state, value) {
        this.setState({
            [state]: value,
        });
    }

    doLogin() {
        const { firstName, gender, employmentStatus, boothName } = this.state;
        const req = {
            firstName: firstName.trim(' '),
            gender: gender.trim(' '),
            employmentStatus: employmentStatus,
            boothName: ['myleripalayam'],
        };
        let data1 = {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(req),
        };
        fetch(
            'https://survey-management-system.herokuapp.com/filter_user/V1.0',
            data1
        )
            .then((response) => {
                this.setState({
                    dataSource: response.data,
                });
                console.log(req);
                console.log(response);
            })
            .catch((error) => {
                console.warn(error);
            });
        this.setModalVisible(false);
    }

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                style={{ flex: 1, flexDirection: 'row', marginBottom: 3 }}
                onPress={() =>
                    this.props.navigation.navigate('ListUserDetail', {
                        book: item,
                    })
                }>
                <View style={{ flex: 1, justifyContent: 'center', marginLeft: 5 }}>
                    <Text style={styles.text}>
                        <Image
                            style={{ height: 20, width: 20 }}
                            source={{
                                uri:
                                    'https://research.cbc.osu.edu/sokolov.8/wp-content/uploads/2017/12/profile-icon-png-898.png',
                            }}
                        />{' '}
                        {item.firstName}
                    </Text>
                    <Text style={styles.text2}>
                        <Image
                            style={{ height: 20, width: 20 }}
                            source={{
                                uri:
                                    'https://www.iconsdb.com/icons/preview/gray/email-2-xxl.png',
                            }}
                        />{' '}
                        {item.email}{' '}
                    </Text>
                    <Text style={styles.text2}>
                        <Image
                            style={{ height: 20, width: 20 }}
                            source={{
                                uri:
                                    'https://cdn3.iconfinder.com/data/icons/maps-and-navigation-colored-1/48/37-512.png',
                            }}
                        />{' '}
                        {item.permanentAddress.city}{' '}
                        <Image
                            style={{ height: 20, width: 20 }}
                            source={{
                                uri:
                                    'https://cdn2.iconfinder.com/data/icons/gaming-and-beyond-part-2-1/80/Phone_gray-512.png',
                            }}
                        />
                        {item.phoneNumber}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    rendersupport = () => {
        return (
            <View
                style={{ height: 1, width: '100%', backgroundColor: '#8E8E8E' }}></View>
        );
    };
    componentDidMount() {

        AsyncStorage.getItem('userToken').then(async (res) => {
            const jwt = await res;
            this.setState({
                jwt_token: jwt,
            });
            getAllData('retrieve_users/V1.0', jwt)
                .then((response) => {

                    this.setState({
                        dataSource: response.data,
                    });


                })
                .catch((error) => {
                })

            getAllData('get_location/V1.0', jwt)
                .then((response) => {

                    this.setState({
                        arr: response.data,
                    });

                })
                .catch((error) => {
                })
        });


    }

    render() {
        const { modalVisible, arr, booths } = this.state;
        const { firstName, gender, employmentStatus, boothName } = this.state;
        return (
            <View style={styles.container}>

                <Modal visible={modalVisible}>
                    <ScrollView style={styles.scrollView}>
                        <View
                            style={{
                                flexDirection: 'row',
                                paddingHorizontal: 12,
                                height: 50,
                                alignItems: 'flex-end',
                            }}>
                            <TouchableOpacity
                                style={{ marginLeft: 8 }}
                                onPress={() => this.setModalVisible(false)}>
                                <Image
                                    source={{
                                        uri:
                                            'https://icon-library.com/images/back-arrow-icon-png/back-arrow-icon-png-1.jpg',
                                    }}
                                    resizeMode="contain"
                                    style={{
                                        width: 25,
                                        height: 25,
                                    }}
                                />
                            </TouchableOpacity>

                            <View
                                style={{
                                    flex: 1,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                }}>
                                <Text style={{ fontSize: 20 }}>Filter</Text>
                            </View>
                        </View>

                        <View style={styles.regform}>
                            <Text> </Text>
                            <Text style={styles.textcolor}> FirstName </Text>
                            <TextInput
                                style={styles.textinput}
                                placeholder="Enter your firstname"
                                underlineColorAndroid={'transparent'}
                                value={firstName}
                                onChangeText={(value) =>
                                    this.onChangeHandle('firstName', value)
                                }
                            />
                            <Text style={styles.textcolor}> Gender </Text>
                            <DropDownPicker
                                items={[
                                    { label: 'Male', value: 'Male' },
                                    { label: 'Female', value: 'Female' },
                                    { label: 'Other', value: 'other' },
                                ]}
                                defaultNull
                                placeholder="Select"
                                placeholderColour="#A9A9A9"
                                containerStyle={{ height: 40 }}
                                onChangeItem={(item) =>
                                    this.onChangeHandle('gender', item.value)
                                }
                            />
                            <Text>{'\n'}</Text>
                            <Text style={styles.textcolor}> EmploymentStatus </Text>
                            <DropDownPicker
                                items={[
                                    { label: 'Employed', value: 'Employed' },
                                    { label: 'Unemployed', value: 'Unemployed' },
                                    { label: 'Student', value: 'Student' },
                                ]}
                                defaultNull
                                placeholder="Select"
                                placeholderColour="#A9A9A9"
                                containerStyle={{ height: 40 }}
                                onChangeItem={(item) =>
                                    this.onChangeHandle('employmentStatus', item.value)
                                }
                            />
                            <Text>{'\n'}</Text>
                            <Text style={styles.textcolor}> BoothName </Text>
                            <TextInput
                                style={styles.textinput}
                                placeholder="Enter your boothname"
                                underlineColorAndroid={'transparent'}
                                value={boothName}
                                onChangeText={(value) =>
                                    this.onChangeHandle('boothName', value)
                                }
                            />
                            <TouchableOpacity style={styles.appButtonContainer}>
                                <Text
                                    style={styles.appButtonText}
                                    secureTextEntry={true}
                                    color="grey"
                                    align="center"
                                    onPress={() => this.doLogin()}>
                                    Apply
                </Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </Modal>
                <View
                    style={{
                        flexDirection: 'row',
                        paddingHorizontal: 12,
                        height: 50,
                        alignItems: 'flex-end',
                    }}>
                    <TouchableOpacity
                        style={{ marginLeft: 8 }}
                        onPress={() => this._onPressBack()}>
                        <Image
                            source={{
                                uri:
                                    'https://icon-library.com/images/back-arrow-icon-png/back-arrow-icon-png-1.jpg',
                            }}
                            resizeMode="contain"
                            style={{
                                width: 25,
                                height: 25,
                            }}
                        />
                    </TouchableOpacity>

                    <View
                        style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 20 }}>Users</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.setModalVisible(true)}>
                        <Image
                            source={{
                                uri:
                                    'https://www.clker.com/cliparts/D/y/Z/a/v/q/filter-button.svg.hi.png',
                            }}
                            resizeMode="contain"
                            style={{
                                width: 75,
                                height: 25,
                            }}
                        />
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={this.state.dataSource}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index}
                    ItemSeparatorComponent={this.rendersupport}
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    text: {
        marginLeft: 12,
        fontSize: 20,
    },
    text2: {
        marginLeft: 12,
        fontSize: 16,
        color: 'gray',
    },
    regform: {
        alignSelf: 'stretch',
    },
    header: {
        fontSize: 30,
        color: '#0f73ee',
        paddingBottom: 5,
        marginBottom: 20,
        paddingLeft: 60,
    },
    textinput: {
        alignSelf: 'stretch',
        height: 40,
        marginBottom: 30,
        color: 'black',
        borderBottomColor: '#A9A9A9',
        borderBottomWidth: 2,
    },
    scrollView: {
        marginHorizontal: 5,
        paddingLeft: 20,
        paddingRight: 30,
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },

    appButtonContainer: {
        elevation: 6,
        backgroundColor: '#0f73ee',
        borderRadius: 6,
        paddingVertical: 8,
        paddingHorizontal: 15,
        width: 350,
        height: 50,
        paddingBottom: 20,
    },
    appButtonText: {
        fontSize: 25,
        color: '#fff',
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    openButton: {
        backgroundColor: '#F194FF',
        borderRadius: 20,
        elevation: 1,
    },
    textStyle: {
        color: 'gray',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
    },
});
export default ListUser;